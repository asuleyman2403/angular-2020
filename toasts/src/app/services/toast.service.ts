import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Toast, ExtToast } from '../models/toast.models';
import { v4 as uuidv4 } from 'uuid';
@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor() {}
  private toastsSource = new BehaviorSubject<ExtToast[]>([]);
  toasts$ = this.toastsSource.asObservable();
  height = 80;
  width = 300;
  verticalPosition = 'BOTTOM';
  horizontalPosition = 'RIGHT';

  updateToastSource(toast: Toast, verticalPosition: string, horizontalPosition: string) {
    this.verticalPosition = verticalPosition;
    this.horizontalPosition = horizontalPosition;
    const toasts = [...this.toastsSource.value, { ...toast, left: `0px`, top: `0px`, id: uuidv4() }];
    this.calculatePositions(toasts);
  }

  deleteFromToastSource(id: string) {
    const toasts = this.toastsSource.value.filter(item => item.id !== id);
    this.calculatePositions(toasts);
  }


  calculatePositions(toasts: ExtToast[]) {
    toasts.forEach((item, index) => {
      console.log(index);
      switch (this.verticalPosition) {
        case 'TOP':
          item.top = index === 0 ? `0px` : `${Number(toasts[index - 1].top.replace(/[A-Za-z]/g, '')) + this.height}px`;
          break;
        case 'BOTTOM':
          item.top =
            index === 0
              ? `${window.innerHeight - this.height}px`
              : `${Number(toasts[index - 1].top.replace(/[A-Za-z]/g, '')) - this.height}px`;
          break;
        case 'CENTER':
          item.top =
            index === 0
              ? `${window.innerHeight / 2 - 40}px`
              : `${Number(toasts[index - 1].top.replace(/[A-Za-z]/g, '')) + this.height}px`;
          break;
        default:
          break;
      }
      switch (this.horizontalPosition) {
        case 'LEFT':
          item.left = '0px';
          break;
        case 'RIGHT':
          item.left = `${window.innerWidth - 320}px`;
          break;
        case 'CENTER':
          item.left = `${window.innerWidth / 2 - 160}px`;
          break;
        default:
          break;
      }
    });
    console.log(toasts);
    this.toastsSource.next(toasts);
  }
}
