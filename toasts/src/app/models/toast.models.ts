export interface Toast {
  title: string;
  type: string;
  content: string;
  duration: number;
  canClose: boolean;
  isDurationVisible: boolean;

}

export interface ExtToast extends Toast {
  id: string;
  left: string;
  top: string;
}
