import { Component, OnInit, Input } from '@angular/core';
import { ExtToast } from 'src/app/models/toast.models';
import { ToastService } from '../../services/toast.service';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent implements OnInit {
  @Input()
  toast: ExtToast;
  initialDuration: number;
  style: { [key: string]: any } = {};
  icon = '';
  constructor(private toastService: ToastService) {}
  ngOnInit() {
    switch (this.toast.type) {
      case 'INFO':
        this.style.backgroundColor = '#0099e5';
        this.icon = 'info';
        break;
      case 'WARNING':
        this.style.backgroundColor = '#fecc47';
        this.icon = 'warning';
        break;
      case 'DANGER':
        this.style.backgroundColor = '#ff4c4c';
        this.icon = 'error_outline';
        break;
      case 'SUCCESS':
        this.style.backgroundColor = '#34bf49';
        this.icon = 'done_all';
        break;
      default:
        this.style.backgroundColor = '#0099e5';
        break;
    }
    this.style.left = this.toast.left;
    this.style.top = this.toast.top;
    this.initialDuration = this.toast.duration;
    if (this.toast.duration > 0) {
      const interval = setInterval(() => {
        this.toast.duration--;
        if (this.toast.duration === 0) {
          this.toastService.deleteFromToastSource(this.toast.id);
          clearInterval(interval);
        }
      }, 1000);
    }

    this.toastService.toasts$.subscribe(data => {
      this.style.left = this.toast.left;
      this.style.top = this.toast.top;
    });
  }

  deleteToast() {
    this.toastService.deleteFromToastSource(this.toast.id);
  }
}
