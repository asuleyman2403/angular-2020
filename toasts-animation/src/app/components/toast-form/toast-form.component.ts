import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Toast } from 'src/app/models/toast.models';
import { ToastService } from '../../services/toast.service';
@Component({
  selector: 'app-toast-form',
  templateUrl: './toast-form.component.html',
  styleUrls: ['./toast-form.component.scss'],
})
export class ToastFormComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private toastService: ToastService) {}
  form: FormGroup;
  types = ['INFO', 'DANGER', 'WARNING', 'SUCCESS'];
  verticalAlignment = ['TOP', 'CENTER', 'BOTTOM'];
  horizontalAlignment = ['LEFT', 'CENTER', 'RIGHT'];

  ngOnInit() {
    this.form = this.formBuilder.group({
      title: [''],
      content: ['', [Validators.required]],
      type: ['INFO', [Validators.required]],
      duration: [0, [Validators.required, Validators.min(0), Validators.max(15)]],
      canClose: [true],
      isDurationVisible: [false],
      verticalPosition: ['BOTTOM', [Validators.required]],
      horizontalPosition: ['RIGHT', [Validators.required]],
    });
  }

  onSubmit(form: FormGroup) {
    this.toastService.updateToastSource(form.value as Toast, form.value.verticalPosition, form.value.horizontalPosition);
  }
}
