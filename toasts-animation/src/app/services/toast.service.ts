import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Toast, ExtToast } from '../models/toast.models';
import { v4 as uuidv4 } from 'uuid';
@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor() {}
  private toastsSource = new BehaviorSubject<ExtToast[]>([]);
  toasts$ = this.toastsSource.asObservable();
  height = 80;
  width = 300;
  animationDuration = '1s';
  verticalPosition = 'BOTTOM';
  horizontalPosition = 'RIGHT';

  updateToastSource(toast: Toast, verticalPosition: string, horizontalPosition: string) {
    this.verticalPosition = verticalPosition;
    this.horizontalPosition = horizontalPosition;
    const toasts = [...this.toastsSource.value, { ...toast, left: `0px`, top: `0px`, id: uuidv4() }];
    this.calculatePositions(toasts);
  }

  deleteFromToastSource(id: string) {
    const toast = this.toastsSource.value.find((item) => item.id === id);
    switch (this.horizontalPosition) {
      case 'LEFT':
        toast.animationName = 'left_reversed';
        break;
      case 'RIGHT':
        toast.animationName = 'right_reversed';
        break;
      case 'CENTER':
        toast.animationName = 'left_reversed';
        break;
      default:
        break;
    }
    const timeout = setTimeout(() => {
      const toasts = this.toastsSource.value.filter((item) => item.id !== id);
      this.calculatePositions(toasts);
      clearTimeout(timeout);
    }, Number(this.animationDuration.replace(/[A-Za-z]/g, '')) * 1000);
  }

  calculatePositions(toasts: ExtToast[]) {
    toasts.forEach((item, index) => {
      switch (this.verticalPosition) {
        case 'TOP':
          item.top = index === 0 ? `0px` : `${Number(toasts[index - 1].top.replace(/[A-Za-z]/g, '')) + this.height}px`;
          break;
        case 'BOTTOM':
          item.top =
            index === 0
              ? `${window.innerHeight - this.height}px`
              : `${Number(toasts[index - 1].top.replace(/[A-Za-z]/g, '')) - this.height}px`;
          break;
        case 'CENTER':
          item.top =
            index === 0
              ? `${window.innerHeight / 2 - 40}px`
              : `${Number(toasts[index - 1].top.replace(/[A-Za-z]/g, '')) + this.height}px`;
          break;
        default:
          break;
      }
      switch (this.horizontalPosition) {
        case 'LEFT':
          item.left = '0px';
          item.animationName = 'left';
          break;
        case 'RIGHT':
          item.left = `${window.innerWidth - 320}px`;
          item.animationName = 'right';
          break;
        case 'CENTER':
          item.left = `${window.innerWidth / 2 - 160}px`;
          item.animationName = 'left';
          break;
        default:
          break;
      }
      item.animationDuration = this.animationDuration;
    });
    console.log(toasts);
    this.toastsSource.next(toasts);
  }
}
