import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ToastFormComponent } from './components/toast-form/toast-form.component';
import { ToastComponent } from './components/toast/toast.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { CustomMaterialModule } from './custom-material.module';

@NgModule({
  declarations: [AppComponent, ToastFormComponent, ToastComponent],
  imports: [BrowserModule, BrowserAnimationsModule, ReactiveFormsModule, CustomMaterialModule, ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
