import { Component, OnInit } from '@angular/core';
import { Toast } from './models/toast.models';
import { ToastService } from './services/toast.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'toasts';
  toasts: Toast[] = [];
  constructor(private toastService: ToastService) {}

  ngOnInit() {
    this.toastService.toasts$.subscribe((data) => {
      this.toasts = data;
    });
  }
}
