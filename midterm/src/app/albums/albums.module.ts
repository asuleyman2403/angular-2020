import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlbumsComponent } from './albums/albums.component';
import { UsersComponent } from './users/users.component';
import { AlbumListComponent } from './album-list/album-list.component';
import { AlbumComponent } from './album/album.component';
import { AlbumsRoutingModule } from './albums-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../custom-material.module';
import { AuthInterceptor } from '../shared/interceptors/auth.interceptor';
import { ApiService } from '../shared/services/api.service';
import { DataService } from '../shared/services/data.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AlbumsProjectionComponent } from './albums-projection/albums-projection.component';
import { AlbumWrapperComponent } from './album-wrapper/album-wrapper.component';

@NgModule({
  declarations: [AlbumsComponent, UsersComponent, AlbumListComponent, AlbumComponent, AlbumsProjectionComponent, AlbumWrapperComponent],
  imports: [CommonModule, AlbumsRoutingModule, HttpClientModule, CustomMaterialModule, ReactiveFormsModule],
  providers: [
    DataService,
    ApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
})
export class AlbumsModule {}
