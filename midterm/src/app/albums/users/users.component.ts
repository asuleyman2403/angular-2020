import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { User } from '@models';
import { Router } from '@angular/router';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  constructor(private dataService: DataService, private router: Router) {}
  users: User[] = [];
  @Input() projected = false;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onProjection = new EventEmitter<User>();

  ngOnInit() {
    this.dataService.getUsers();
    this.dataService.usersSource.subscribe((users) => (this.users = users));
  }

  selectUser(user: User) {
    if (this.projected) {
      this.onProjection.emit(user);
    } else {
      this.router.navigate(['/albums'], { queryParams: { userId: user.id } });
    }
  }
}
