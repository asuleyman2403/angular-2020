import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumsComponent } from './albums/albums.component';
import { AlbumComponent } from './album/album.component';
import { AlbumListComponent } from './album-list/album-list.component';
import { AlbumsProjectionComponent } from './albums-projection/albums-projection.component';

const routes: Routes = [
  {
    path: 'projection',
    component: AlbumsProjectionComponent
  },
  {
    path: '',
    component: AlbumsComponent,
    children: [
      {
        path: '',
        component: AlbumListComponent,
      },
      {
        path: ':id',
        component: AlbumComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class AlbumsRoutingModule {}
