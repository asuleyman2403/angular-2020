import { Component, OnInit, Input } from '@angular/core';
import { Album, Photo } from '@models';
import { ApiService } from '../../shared/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss'],
})
export class AlbumComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router, private apiService: ApiService) {}
  @Input() album?: Album;
  photos: Photo[] = [];
  dataSource: Array<Photo> = [];
  pageSizeOptions: number[] = [5, 10, 25, 100];
  public pageSize = 5;
  public currentPage = 0;
  ngOnInit() {
    if (this.album) {
      // this.getPhotos();
    } else {
      this.route.params.subscribe((params) => {
        const id = params.id ? Number(params.id) : null;
        if (id) {
          this.apiService
            .getAlbum(id)
            .toPromise()
            .then((album) => {
              this.album = album;
              // this.getPhotos();
            });
        } else {
          this.router.navigate(['/albums']);
        }
      });
    }
  }

  getPhotos() {
    this.apiService
      .getPhotos(this.album.id)
      .toPromise()
      .then((photos) => {
        this.photos = photos;
        this.iterator();
      });
  }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.photos.slice(start, end);
    this.dataSource = part;
  }
}
