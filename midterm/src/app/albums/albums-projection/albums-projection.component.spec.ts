import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumsProjectionComponent } from './albums-projection.component';

describe('AlbumsProjectionComponent', () => {
  let component: AlbumsProjectionComponent;
  let fixture: ComponentFixture<AlbumsProjectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbumsProjectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumsProjectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
