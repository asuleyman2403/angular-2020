import { Component, OnInit } from '@angular/core';
import { Album, User } from '@models';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-albums-projection',
  templateUrl: './albums-projection.component.html',
  styleUrls: ['./albums-projection.component.scss'],
})
export class AlbumsProjectionComponent implements OnInit {
  constructor(private apiService: ApiService) {}
  albums: Album[] = [];
  ngOnInit() {
    this.apiService
      .getAlbums()
      .toPromise()
      .then((albums) => {
        this.albums = albums;
      });
  }

  getAlbums(user: User) {
    this.apiService
      .getAlbums(user.id)
      .toPromise()
      .then((albums) => {
        this.albums = albums;
      });
  }
}
