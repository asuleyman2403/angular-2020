import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Album } from '@models';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.scss'],
})
export class AlbumListComponent implements OnInit {
  constructor(private router: Router, private route: ActivatedRoute, private apiService: ApiService) {}
  albums: Album[] = [];
  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      const userId = params.userId ? params.userId : null;

      this.apiService
        .getAlbums(userId)
        .toPromise()
        .then((albums) => {
          this.albums = albums;
        });
    });
  }
}
