import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthToken } from '@models';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem('token')) as AuthToken;
    if (token) {
      const authReq = req.clone({ headers: req.headers.set('Authorization', token.access_token) });
      return next.handle(authReq);
    }
    return next.handle(req);
  }
}
