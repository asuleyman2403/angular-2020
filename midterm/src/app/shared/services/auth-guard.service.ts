import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate, CanLoad {
  constructor(private router: Router) {}

  canActivate(): boolean {
    return this.checkToken();
  }

  canLoad(): boolean {
    return this.checkToken();
  }

  checkToken(): boolean {
    const token = localStorage.getItem('token');
    if (token) {
      return true;
    }

    this.router.navigate(['/auth']);

    return false;
  }
}
