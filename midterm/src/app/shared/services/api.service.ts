import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Post, Album, Comment, User, Photo } from '@models';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  logout() {
    return this.http.post(`${environment.BASE_URL}/sign-out/`, {});
  }

  getPosts() {
    return this.http.get<Post[]>(`${environment.BASE_URL}/posts/`);
  }

  createPost(post: any) {
    return this.http.post(`${environment.BASE_URL}/posts/`, post);
  }

  getComments(postId: number) {
    return this.http.get<Comment[]>(`${environment.BASE_URL}/posts/${postId}/comments`);
  }

  createComment(postId: number, comment) {
    return this.http.post(`${environment.BASE_URL}/posts/${postId}/comments`, comment);
  }

  getAlbums(userId?: number) {
    return this.http.get<Album[]>(`${environment.BASE_URL}/albums${userId ? `?userId=${userId}` : ''}`);
  }

  getAlbum(albumId: number) {
    return this.http.get<Album>(`${environment.BASE_URL}/albums/${albumId}`);
  }

  getPhotos(albumId: number) {
    return this.http.get<Photo[]>(`${environment.BASE_URL}/albums/${albumId}/photos`);
  }

  getUsers() {
    return this.http.get<User[]>(`${environment.BASE_URL}/users`);
  }
}
