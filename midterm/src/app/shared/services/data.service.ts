import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Post, Album, User } from '@models';
import { ApiService } from './api.service';
@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private apiService: ApiService) {}
  public postsSource = new BehaviorSubject<Post[]>([]);
  public filterKeySource = new BehaviorSubject<string>('');
  public usersSource = new BehaviorSubject<User[]>([]);
  updatePosts() {
    this.apiService
      .getPosts()
      .toPromise()
      .then((data) => this.postsSource.next(data));
  }

  getUsers() {
    this.apiService
      .getUsers()
      .toPromise()
      .then((data) => {
        this.usersSource.next(data);
      });
  }
}
