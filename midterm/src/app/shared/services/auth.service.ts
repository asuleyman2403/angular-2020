import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthCredentials } from '@models';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  register(data) {
    return this.http.post(`${environment.BASE_URL}/users/`, data);
  }

  login(data: AuthCredentials) {
    return this.http.post(`${environment.BASE_URL}/sign-in/`, data);
  }

}
