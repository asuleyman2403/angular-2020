import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor(private router: Router, private location: Location) {}
  links = ['posts', 'albums', 'albums/projection', 'auth/logout'];
  activeLink = 'posts';
  ngOnInit() {
    this.router.events.subscribe(() => {
      const path = this.location.path();
      if (path === '/posts') {
        this.activeLink = 'posts';
      }
      if (path === '/albums') {
        this.activeLink = 'albums';
      }
      if (path === '/albums/projection') {
        this.activeLink = 'albums/projection';
      }
      if (path === 'auth/logout') {
        this.activeLink = 'auth/logout';
      }
    });
  }

  onClick(link) {
    console.log(link);
    this.router.navigate([link]);
  }
}
