import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../custom-material.module';
import { PostsRoutingModule } from './posts-routing.module';
import { AuthInterceptor } from '../shared/interceptors/auth.interceptor';
import { ApiService } from '../shared/services/api.service';
import { DataService } from '../shared/services/data.service';
import { ReactiveFormsModule } from '@angular/forms';
import { PostListComponent } from './post-list/post-list.component';
import { PostComponent } from './post/post.component';
import { PostCreateComponent } from './post-create/post-create.component';
import { CommentsComponent } from './comments/comments.component';
import { CommentCreateComponent } from './comment-create/comment-create.component';
import { PostFilterComponent } from './post-filter/post-filter.component';

@NgModule({
  declarations: [PostListComponent, PostComponent, PostCreateComponent, CommentsComponent, CommentCreateComponent, PostFilterComponent],
  imports: [CommonModule, HttpClientModule, PostsRoutingModule, CustomMaterialModule, ReactiveFormsModule],
  providers: [
    DataService,
    ApiService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
})
export class PostsModule {}
