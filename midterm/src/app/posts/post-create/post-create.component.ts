import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../shared/services/api.service';
import { DataService } from '../../shared/services/data.service';
import { AuthToken } from '@models';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.scss'],
})
export class PostCreateComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private apiService: ApiService, private dataService: DataService) {}
  form: FormGroup;
  ngOnInit() {
    this.form = this.formBuilder.group({
      title: ['', [Validators.required]],
      body: ['', [Validators.required]],
    });
  }

  onSubmit(value) {
    const { title, body } = value;
    const { userId } = JSON.parse(localStorage.getItem('token')) as AuthToken;
    this.apiService
      .createPost({ title, body, userId })
      .toPromise()
      .then(() => {
        this.dataService.updatePosts();
      });
  }
}
