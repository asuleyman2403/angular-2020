import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { DataService } from '../../shared/services/data.service';
import { Post } from '@models';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss'],
})
export class PostListComponent implements OnInit {
  constructor(private dataService: DataService) {}
  posts: Array<Post> = [];
  dataSource: Array<Post> = [];
  pageSizeOptions: number[] = [5, 10, 25, 100];
  public pageSize = 10;
  public currentPage = 0;

  ngOnInit() {
    this.dataService.updatePosts();
    this.dataService.getUsers();
    this.filterPostsByUser();
  }

  filterPostsByUser() {
    this.dataService.postsSource.subscribe((posts) => {
      this.dataService.filterKeySource.subscribe(filterKey => {
        let filteredUserIds = [];
        this.dataService.usersSource.subscribe(users => {
          filteredUserIds = users.filter(user => user.name.includes(filterKey)).map(user => user.id);
          this.posts = posts.filter(post => filteredUserIds.includes(post.userId)).reverse();
          this.iterator();
        });
      });
    });
  }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.posts.slice(start, end);
    this.dataSource = part;
  }
}
