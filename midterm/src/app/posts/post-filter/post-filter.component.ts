import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { FormBuilder, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-post-filter',
  templateUrl: './post-filter.component.html',
  styleUrls: ['./post-filter.component.scss'],
})
export class PostFilterComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private dataService: DataService) {}
  form: FormGroup;
  ngOnInit() {
    this.form = this.formBuilder.group({
      username: '',
    });

    this.form.get('username').valueChanges.subscribe((value) => {
      this.dataService.filterKeySource.next(value);
    });
  }
}
