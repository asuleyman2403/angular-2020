import { Component, OnInit, Input } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { DataService } from '../../shared/services/data.service';
import { Post, Comment, User, AuthToken } from '@models';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent implements OnInit {
  constructor(private apiService: ApiService, private dataService: DataService, private formBuilder: FormBuilder) {}
  @Input() post: Post;
  comments: Comment[] = [];
  username = '';
  myEmail = '';
  showComments = false;
  loading = false;
  form: FormGroup;
  ngOnInit() {
    this.dataService.usersSource.subscribe((users) => {
      const user = users.find((u: User) => this.post.userId === u.id);

      this.username = user ? user.name : this.username;
      const token = JSON.parse(localStorage.getItem('token')) as AuthToken;
      const me = users.find((u: User) => this.post.userId === token.userId);
      this.myEmail = me ? me.email : this.myEmail;
    });
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      body: ['', Validators.required],
    });
  }

  toggleShowComments() {
    this.showComments = !this.showComments;
    if (this.showComments) {
      this.apiService
        .getComments(this.post.id)
        .toPromise()
        .then((data) => {
          this.comments = data.reverse();
        });
    }
  }

  onSubmit(value) {
    const { title, body } = value;

    this.apiService
      .createComment(this.post.id, { title, body, postId: +this.post.id, email: this.myEmail })
      .toPromise()
      .then(() => {
        this.showComments = false;
        this.toggleShowComments();
      });
  }
}
