import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../shared/services/api.service';
@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {
  constructor(private router: Router, private apiService: ApiService) {}

  ngOnInit() {
    // this.apiService.logout().subscribe(() => {
    //   localStorage.clear();
    //   this.router.navigate(['/auth/login']);
    // });
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
}
