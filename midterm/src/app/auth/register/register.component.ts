import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { AuthToken } from '@models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}
  form: FormGroup;
  ngOnInit() {
    this.form = this.formBuilder.group({
      name: ['', [Validators.required]],
      username: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      address: this.formBuilder.group({
        street: ['', [Validators.required]],
        suite: ['', [Validators.required]],
        city: ['', [Validators.required]],
        zipcode: ['', [Validators.required]],
        geo: this.formBuilder.group({
          lat: [''],
          lng: [''],
        }),
      }),
      phone: ['', Validators.required],
      website: ['', Validators.required],
      company: this.formBuilder.group({
        name: ['', Validators.required],
        catchPhrase: ['', Validators.required],
        bs: ['', Validators.required],
      }),
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    console.log(this.form.value);
    const { username, password } = this.form.value;
    this.authService.register(this.form.value).subscribe(() => {
      this.authService.login({username, password}).subscribe((data: AuthToken) => {
        if (data && data.access_token) {
          localStorage.setItem('token', JSON.stringify(data));
          this.router.navigate(['/posts']);
        }
      });
    });
  }

}
