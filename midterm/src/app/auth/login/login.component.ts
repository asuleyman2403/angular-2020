import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../shared/services/auth.service';
import { AuthToken } from '@models';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router) {}
  form: FormGroup;

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const username = this.form.get('username').value.trim();
    const password = this.form.get('password').value.trim();

    this.authService.login({ username, password }).subscribe((data: AuthToken) => {
      if (data.access_token) {
        localStorage.setItem('token', JSON.stringify(data));
        this.router.navigate(['/posts']);
      }
    });
  }
}
