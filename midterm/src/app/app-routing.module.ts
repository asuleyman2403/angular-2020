import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService as AuthGuard } from './shared/services/auth-guard.service';
import { MainLayoutComponent } from './main-layout/main-layout.component';
const routes: Routes = [
  {
    path: 'auth',
    loadChildren: async () => {
      const authModule = await import('./auth/auth.module').then((m) => m.AuthModule);
      return authModule;
    },
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'posts',
        loadChildren: async () => {
          const postsModule = await import('./posts/posts.module').then((m) => m.PostsModule);
          return postsModule;
        },
        canLoad: [AuthGuard],
      },
      {
        path: 'albums',
        loadChildren: async () => {
          const albumsModule = await import('./albums/albums.module').then((m) => m.AlbumsModule);
          return albumsModule;
        },
        canLoad: [AuthGuard],
      },
    ],
  },
  {
    path: '**',
    redirectTo: 'posts',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
