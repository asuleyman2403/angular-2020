import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { CalcButtonComponent } from './components/calc-button/calc-button.component';
import { CalculatorService } from './services/calculator.service';
import { CalcDisplayComponent } from './components/calc-display/calc-display.component';
@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    CalcButtonComponent,
    CalcDisplayComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    CalculatorService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
