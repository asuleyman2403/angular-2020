import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  private notationSource = new BehaviorSubject<string>('0');
  notation$ = this.notationSource.asObservable();

  constructor() { }

  setNotationSource(notation: string) {
    this.notationSource.next(notation);
  }

}
