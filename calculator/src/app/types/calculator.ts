export enum CalculatorState {
  NUMBER_TYPED = 'NUMBER_TYPED',
  OPERATION_TYPED = 'OPERATION_TYPED',
  UNARY_OPERATION_TYPED = 'UNARY_OPERATION_TYPED',
  SEPARATOR_TYPED = 'SEPARATOR_TYPED',
}

export enum ButtonTypes {
  NUM = 'NUMBER',
  SEPARATOR = 'SEPARATOR',
  SIGN = 'SIGN',
  OPERATION = 'OPERATION',
  UNARY_OPERATION = 'UNARY_OPERATION',
  EQUAL = 'EQUAL',
}

export interface ButtonParam {
  text: string;
  description: string;
  disabled: boolean;
  class: string;
  type: ButtonTypes;
}
