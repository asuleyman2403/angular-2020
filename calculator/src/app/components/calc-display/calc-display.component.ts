import { Component, OnInit, OnDestroy } from '@angular/core';
import { pipe, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { CalculatorService } from '../../services/calculator.service';

@Component({
  selector: 'app-calc-display',
  templateUrl: './calc-display.component.html',
  styleUrls: ['./calc-display.component.scss'],
})
export class CalcDisplayComponent implements OnInit, OnDestroy {
  private destroyed$ = new Subject<void>();

  constructor(private calcService: CalculatorService) {}

  prev = '';
  current = '';

  ngOnInit() {
    this.calcService.notation$.pipe(takeUntil(this.destroyed$)).subscribe((notation) => {
      const notationArr = notation.split(' ');
      this.prev = notationArr.length > 1 ? notationArr.slice(0, notationArr.length - 1).join(' ') : '';
      this.current = notationArr[notationArr.length - 1];
    });
  }

  ngOnDestroy() {
    this.destroyed$.next();
    this.destroyed$.complete();
  }
}
