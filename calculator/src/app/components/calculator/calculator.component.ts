import { Component, OnInit } from '@angular/core';
import { initialButtonParams } from '../../constants/buttonts';
import { ButtonParam, CalculatorState, ButtonTypes } from '../../types/calculator';
import { CalculatorService } from '../../services/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss'],
})
export class CalculatorComponent implements OnInit {
  private prevParam: ButtonParam | null = null;
  private currentParam: ButtonParam | null = null;
  private state = CalculatorState.NUMBER_TYPED;
  private notation: string;
  constructor(private calcService: CalculatorService) {}

  buttonParams: Array<ButtonParam> = initialButtonParams;

  ngOnInit() {
    this.calcService.notation$.subscribe((notation) => {
      this.notation = notation;
      console.log(notation);
    });
  }

  calcButtonClick($event: ButtonParam | null) {
    this.prevParam = this.currentParam;
    this.currentParam = $event;
    this.updateNotation();
    this.changeCalculatorState();
  }

  changeCalculatorState() {
    switch (this.currentParam.type) {
      case ButtonTypes.OPERATION:
        this.state = CalculatorState.OPERATION_TYPED;
        break;
      case ButtonTypes.NUM:
        this.state = CalculatorState.NUMBER_TYPED;
        break;
      case ButtonTypes.UNARY_OPERATION:
        this.state = CalculatorState.UNARY_OPERATION_TYPED;
        break;
      case ButtonTypes.SEPARATOR:
        this.state = CalculatorState.SEPARATOR_TYPED;
        break;
      default:
        break;
    }
    this.disableNeededButtons();
  }

  disableNeededButtons() {
    this.buttonParams = initialButtonParams.map((item) => {
      let res = item;
      switch (this.state) {
        case CalculatorState.OPERATION_TYPED:
          if (
            item.type === ButtonTypes.EQUAL ||
            item.type === ButtonTypes.OPERATION ||
            item.type === ButtonTypes.UNARY_OPERATION ||
            item.type === ButtonTypes.SEPARATOR
          ) {
            res = { ...item, disabled: true };
          }
          if (item.description === 'C') {
            res = { ...item, disabled: false };
          }
          break;
        case CalculatorState.SEPARATOR_TYPED:
          if (
            item.type === ButtonTypes.EQUAL ||
            item.type === ButtonTypes.OPERATION ||
            item.type === ButtonTypes.UNARY_OPERATION ||
            item.type === ButtonTypes.SIGN
          ) {
            res = { ...item, disabled: true };
          }
          break;
        case CalculatorState.UNARY_OPERATION_TYPED:
            if (this.currentParam.description === '%' && !(item.type === ButtonTypes.NUM || item.description === 'C')) {
              res = { ...item, disabled: true };
            }
            break;
        default:
          break;
      }
      return res;
    });
  }

  updateNotation() {
    const { description } = this.currentParam;
    const ntArr = this.notation.split(' ');
    switch (description) {
      case 'SQR':
        this.wrapLastNumberWithFunction('sqr');
        break;
      case 'SQRT':
        this.wrapLastNumberWithFunction('sqrt');
        break;
      case 'CE':
        if (ntArr[ntArr.length - 1] !== '0') {
          this.calcService.setNotationSource(`${ntArr.slice(0, ntArr.length - 1).join(' ')} 0`);
        }
        break;
      case 'C':
        this.calcService.setNotationSource('0');
        break;
      case 'DELETE':
        if (ntArr[ntArr.length - 1] !== '0') {
          const last = ntArr[ntArr.length - 1];
          this.calcService.setNotationSource(
            `${ntArr.slice(0, ntArr.length - 1).join(' ')} ${last.length === 1 ? '0' : last.slice(0, last.length - 1)}`,
          );
        }
        break;
      case 'OVER_ONE':
        this.appendOverHalf();
        break;
      case '%':
        this.calcService.setNotationSource(this.notation + '%');
        break;
      case 'SIGN':
        if (ntArr[ntArr.length - 1] !== '0') {
          this.calcService.setNotationSource(
            `${ntArr.slice(0, ntArr.length - 1).join(' ')} ${-1 * Number(ntArr[ntArr.length - 1])}`,
          );
        }
        break;
      case 'SEPARATOR':
        this.calcService.setNotationSource(`${this.notation}.`);
        break;
      case '=':
        this.calculate();
        break;
      default:
        if (this.currentParam.type === ButtonTypes.NUM) {
          this.appendNumber(description);
        }
        if (this.currentParam.type === ButtonTypes.OPERATION) {
          this.appendOperation(description);
        }
        break;
    }
  }

  calculate() {
    let result = 0;
    let notation = this.notation.trim();
    while (notation.includes('%')) {
      const notArr = notation.split(' ');
      for (let i = 0; i < notArr.length; i++) {
        if (notArr[i].includes('%')) {
          let percentNumbers = notArr[i].split('%');
          notArr[i] = `${Number(percentNumbers[0]) * Number(percentNumbers[1]) / 100}`;
        }
      }
      notation = notArr.join(' ');
    }
    while (notation.includes('sqrt')) {
      const index = notation.indexOf('sqrt(');
      for (let i = index + 4; i < notation.length; i++) {
        if (notation[i] === ')') {
          const num = +notation.substring(index, i + 1).replace(/\D/g, '');
          notation =
            notation.substring(0, index) +
            `${Math.round(Math.sqrt(num) * 1000) / 1000}` +
            notation.substring(i + 1, notation.length);
          break;
        }
      }
    }
    while (notation.includes('sqr')) {
      const index = notation.indexOf('sqr(');
      for (let i = index + 4; i < notation.length; i++) {
        if (notation[i] === ')') {
          const num = +notation.substring(index, i + 1).replace(/\D/g, '');
          notation = notation.substring(0, index) + `${num * num}` + notation.substring(i + 1, notation.length);
          break;
        }
      }
    }

    let intermediateValues = notation.split(' ');
    for (let i = 0; i < intermediateValues.length; i++) {
      if (i !== 0 && intermediateValues[i - 1] === '-') {
        if (!intermediateValues[i].includes('-')) {
          intermediateValues[i] = '-' + intermediateValues[i];
        }
        intermediateValues[i - 1] = '+';
      }
    }
    intermediateValues = intermediateValues.join('').split('+');
    for (let i = 0; i < intermediateValues.length; i++) {
      if (intermediateValues[i].includes('x') || intermediateValues[i].includes('/')) {
        const arr = intermediateValues[i].split('x');
        let innerRes = 1;
        for (let j = 0; j < arr.length; j++) {
          if (arr[j].includes('/')) {
            let divs = arr[j].split('/');
            let divRes = Number(divs[0]);
            divs = divs.slice(1, arr[j].length);
            divs.forEach((item) => {
              divRes = divRes / Number(item);
            });
            arr[j] = `${divRes}`;
          }
          innerRes *= Number(arr[j]);
        }
        intermediateValues[i] = `${innerRes}`;
      }
      result += Number(intermediateValues[i]);
    }
    this.calcService.setNotationSource(`${Math.round(result * 1000) / 1000}`);
  }

  appendOperation(description: string) {
    this.calcService.setNotationSource(`${this.notation} ${description} `);
  }

  wrapLastNumberWithFunction(fName: string) {
    const ntArr = this.notation.split(' ');
    this.calcService.setNotationSource(
      `${ntArr.slice(0, ntArr.length - 1).join(' ')} ${fName}(${ntArr[ntArr.length - 1]})`,
    );
  }

  appendNumber(description: string) {
    const ntArr = this.notation.split(' ');
    if (ntArr[ntArr.length - 1] === '0') {
      if (ntArr.length > 1) {
        this.calcService.setNotationSource(`${ntArr.slice(0, ntArr.length - 1).join(' ')} ${description}`);
      } else {
        this.calcService.setNotationSource(`${ntArr.slice(0, ntArr.length - 1).join(' ')}${description}`);
      }
    } else {
      this.calcService.setNotationSource(`${this.notation}${description}`);
    }
  }

  appendOverHalf() {
    const ntArr = this.notation.split(' ');
    if (ntArr.length > 1) {
      this.calcService.setNotationSource(
        `${ntArr.slice(0, ntArr.length - 1).join(' ')} 1 / ${ntArr[ntArr.length - 1]}`,
      );
    } else {
      this.calcService.setNotationSource(`1 / ${ntArr[0]}`);
    }
  }
}
