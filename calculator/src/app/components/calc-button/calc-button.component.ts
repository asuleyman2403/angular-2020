import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ButtonParam } from '../../types/calculator';
@Component({
  selector: 'app-calc-button',
  templateUrl: './calc-button.component.html',
  styleUrls: ['./calc-button.component.scss'],
})
export class CalcButtonComponent implements OnInit {
  constructor() {}

  @Input() param: ButtonParam;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onButtonClick = new EventEmitter<ButtonParam>();

  ngOnInit() {}

  onClick() {
    if (this.param.disabled) {
      return;
    }
    this.onButtonClick.emit(this.param);
  }
}
