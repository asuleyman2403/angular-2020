import { ButtonParam, ButtonTypes } from '../types/calculator';


export const initialButtonParams: Array<ButtonParam> = [
  {
    text: '%',
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: '%',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: 'CE',
    text: 'CE',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: 'C',
    text: 'C',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: 'DELETE',
    text: '⌫',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: 'OVER_ONE',
    text: '<sup>1</sup> / <sub>X</sub>',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: 'SQR',
    text: 'x<sup>2</sup>',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.UNARY_OPERATION,
    description: 'SQRT',
    text: '√‎x',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.OPERATION,
    description: '/',
    text: '/',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '7',
    text: '7',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '8',
    text: '8',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '9',
    text: '9',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.OPERATION,
    description: 'x',
    text: 'x',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '4',
    text: '4',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '5',
    text: '5',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '6',
    text: '6',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.OPERATION,
    description: '-',
    text: '-',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '1',
    text: '1',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '2',
    text: '2',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '3',
    text: '3',
  },
  {
    disabled: false,
    class: 'btn--operational',
    type: ButtonTypes.OPERATION,
    description: '+',
    text: '+',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.SIGN,
    description: 'SIGN',
    text: '±',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.NUM,
    description: '0',
    text: '0',
  },
  {
    disabled: false,
    class: 'btn--num',
    type: ButtonTypes.SEPARATOR,
    description: 'SEPARATOR',
    text: '.',
  },
  {
    disabled: false,
    class: 'btn--eq',
    type: ButtonTypes.EQUAL,
    description: '=',
    text: '=',
  },
];
