import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { AuthGuardService as AuthGuard } from './shared/services/auth-guard.service';
import { AdminGuardService as AdminGuard } from './shared/services/admin-guard.service';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: async () => {
      const authModule = await import('./auth/auth.module').then((m) => m.AuthModule);
      return authModule;
    },
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: 'store',
        loadChildren: async () => {
          const storeModule = await import('./store/store.module').then((m) => m.StoreModule);
          return storeModule;
        },
        canLoad: [AuthGuard],
      },
      {
        path: 'dashboard',
        loadChildren: async () => {
          const dashboardModule = await import('./dashboard/dashboard.module').then((m) => m.DashboardModule);
          return dashboardModule;
        },
        canLoad: [AdminGuard],
      },
      {
        path: '**',
        redirectTo: 'store'
      }
    ],
  },
  {
    path: '**',
    redirectTo: 'auth',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
