import { Component, OnInit } from '@angular/core';
import { User } from '@models';
@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  constructor() { }
  user: User;
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('currentUser')) as User;
  }

}
