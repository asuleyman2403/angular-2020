export interface AuthCredentials {
  username: string;
  password: string;
}

export interface User {
  id: string;
  name: string;
  username: string;
  email: string;
  password: string;
  phone: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipCode: string;
  };
  isAdmin?: boolean;
}
