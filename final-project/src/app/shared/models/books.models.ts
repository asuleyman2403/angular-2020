export enum Currency {
  KZT = 'KZT',
  RUB = 'RUB',
  USD = 'USD',
}

export interface Price {
  cost: number;
  currency: Currency;
}

export interface Author {
  id: string;
  name: string;
  surname: string;
}

export interface Book {
  id: string;
  title: string;
  imageUrl: string;
  releaseDate: Date;
  price: Price;
  author: Author;
  genres: string[];
}
