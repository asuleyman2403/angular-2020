import { Book } from './books.models';
import { User } from './auth.models';

export interface Basket {
  user: User | null;
  books: Book[];
  checkedOut: Book[];
}
