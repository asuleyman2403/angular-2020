import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User, Book } from '@models';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}
  allBooks: Book[] = [];
  filteredBookSource = new BehaviorSubject<Book[]>([]);
  filteredBooks = this.filteredBookSource.asObservable();

  loadData() {
    this.http.get('http://localhost:5500/users.json').toPromise().then((users) => {
      if (!this.checkItem('users')) {
        localStorage.setItem('users', JSON.stringify(users));
      }
    });

    this.http.get('http://localhost:5500/books.json').toPromise().then((books) => {
      if (!this.checkItem('books')) {
        localStorage.setItem('books', JSON.stringify(books));
        this.allBooks = books as Book[];
        this.filteredBookSource.next(this.allBooks);
      } else {
        this.allBooks = JSON.parse(localStorage.getItem('books')) as Book[];
        this.filteredBookSource.next(this.allBooks);
      }
    });

    this.http.get('http://localhost:5500/genres.json').toPromise().then((genres) => {
      if (!this.checkItem('genres')) {
        localStorage.setItem('genres', JSON.stringify(genres));
      }
    });

    this.http.get('http://localhost:5500/authors.json').toPromise().then((authors) => {
      if (!this.checkItem('authors')) {
        localStorage.setItem('authors', JSON.stringify(authors));
      }
    });
  }

  private checkItem(item: string) {
    const objectItem = JSON.parse(localStorage.getItem(item));
    return objectItem ? true : false;
  }
}
