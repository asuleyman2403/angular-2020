import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { User } from '@models';

@Injectable({
  providedIn: 'root',
})
export class AdminGuardService implements CanActivate, CanLoad {
  constructor(private router: Router) {}

  canActivate(): boolean {
    return this.checkUser();
  }

  canLoad(): boolean {
    return this.checkUser();
  }

  checkUser(): boolean {
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) as User;
    if (currentUser && currentUser.isAdmin) {
      return true;
    }

    this.router.navigate(['/auth']);

    return false;
  }
}
