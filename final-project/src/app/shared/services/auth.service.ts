import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthCredentials } from '@models';
import { User } from '@models';
import { v4 as uuv4 } from 'uuid';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  register(data) {
    const users = JSON.parse(localStorage.getItem('users')) as User[];
    const id = uuv4();
    users.push({ id, ...data});
    localStorage.setItem('users', JSON.stringify(users));
  }

  login(data: AuthCredentials) {
    const { username, password } = data;
    const users = JSON.parse(localStorage.getItem('users')) as User[];
    return users.find((user) => user.username === username && user.password === password);
  }

}
