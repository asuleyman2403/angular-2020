import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookListComponent } from './book-list/book-list.component';
import { BasketComponent } from './basket/basket.component';
import { BookFilterComponent } from './book-filter/book-filter.component';
import { MainComponent } from './main/main.component';
import { BookComponent } from './book/book.component';
import { BookSingleComponent } from './book-single/book-single.component';
import { StoreRoutingModule } from './store-routing.module';
import { CustomMaterialModule } from '../custom-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { BasketBookComponent } from './basket-book/basket-book.component';

@NgModule({
  declarations: [
    BookListComponent,
    BasketComponent,
    BookFilterComponent,
    MainComponent,
    BookComponent,
    BookSingleComponent,
    BasketBookComponent,
  ],
  imports: [CommonModule, StoreRoutingModule, CustomMaterialModule, ReactiveFormsModule],
})
export class StoreModule {}
