import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Book } from '@models';
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  constructor(private dataService: DataService) { }
  books: Book[] = [];
  dataSource: Array<Book> = [];
  pageSizeOptions: number[] = [4, 8, 16, 32, 48];
  public pageSize = 10;
  public currentPage = 0;
  ngOnInit() {
    this.dataService.filteredBooks.subscribe(books => {
      this.books = books;
      this.iterator();
    });
  }

  public handlePage(e: any) {
    this.currentPage = e.pageIndex;
    this.pageSize = e.pageSize;
    this.iterator();
  }

  private iterator() {
    const end = (this.currentPage + 1) * this.pageSize;
    const start = this.currentPage * this.pageSize;
    const part = this.books.slice(start, end);
    this.dataSource = part;
  }
}
