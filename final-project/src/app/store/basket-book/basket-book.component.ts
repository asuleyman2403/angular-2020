import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book, Basket, User } from '@models';

@Component({
  selector: 'app-basket-book',
  templateUrl: './basket-book.component.html',
  styleUrls: ['./basket-book.component.scss']
})
export class BasketBookComponent implements OnInit {
  constructor() {}
  @Input() book;
  @Input() bought;
  @Output() buy = new EventEmitter<Book>();
  @Output() remove = new EventEmitter<Book>();
  ngOnInit() {}

  buyBook() {
    this.buy.emit(this.book);
  }

  removeBook() {
    this.remove.emit(this.book);
  }
}
