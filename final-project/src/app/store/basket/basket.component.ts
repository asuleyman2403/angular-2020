import { Component, OnInit } from '@angular/core';
import { Basket, User, Book } from '@models';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {

  constructor() { }
  basket: Basket;
  books: Book[];
  checkout: Book[];
  ngOnInit() {
    const basket = JSON.parse(localStorage.getItem('basket')) as Basket | null;
    if (basket) {
      this.basket = basket;
    } else {
      const nb: Basket = { books: [], checkedOut: [], user: null };
      const currentUser = JSON.parse(localStorage.getItem('currentUser')) as User | null;
      if (currentUser) {
        nb.user = currentUser;
      }
      localStorage.setItem('basket', JSON.stringify(nb));
      this.basket = nb;
    }
    this.books = this.basket.books;
    this.checkout = this.basket.checkedOut;
  }

  handleCheckoutClick(book: Book) {
    this.books = this.books.filter(item => item.id !== book.id);
    this.checkout.push(book);
    this.basket.books = this.books;
    this.basket.checkedOut = this.checkout;
    localStorage.setItem('basket', JSON.stringify(this.basket));
  }

  handleRemoveClick(book: Book) {
    this.books = this.books.filter(item => item.id !== book.id);
    this.basket.books = this.books;
    localStorage.setItem('basket', JSON.stringify(this.basket));
  }
}
