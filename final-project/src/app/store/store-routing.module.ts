import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BasketComponent } from './basket/basket.component';
import { BookSingleComponent } from './book-single/book-single.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
  },
  {
    path: 'basket',
    component: BasketComponent
  },
  // {
  //   path: 'book/:id',
  //   component: BookSingleComponent
  // },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [],
  })
export class StoreRoutingModule { }
