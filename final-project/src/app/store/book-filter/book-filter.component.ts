import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
@Component({
  selector: 'app-book-filter',
  templateUrl: './book-filter.component.html',
  styleUrls: ['./book-filter.component.scss']
})
export class BookFilterComponent implements OnInit {

  constructor(private dataService: DataService) { }
  genres: any[] = [];
  ngOnInit() {
    const genres = JSON.parse(localStorage.getItem('genres')) as string[];
    this.genres = genres.map((genre, index) => ({ index, name: genre, selected: false}));
  }

  handleGenreChange(genre: any) {
    genre.selected = !genre.selected;
    const genres = this.genres.filter((item) => item.selected).map(item => item.name);
    const filteredBooks = this.dataService.allBooks.filter(book => {
      return book.genres.reduce((includes, item) => {
        if (!genres.length) {
          return true;
        }
        if (!includes) {
          return genres.includes(item);
        } else {
          return true;
        }
      }, false);
    });
    this.dataService.filteredBookSource.next(filteredBooks);
  }

}
