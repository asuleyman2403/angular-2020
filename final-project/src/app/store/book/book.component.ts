import { Component, OnInit, Input } from '@angular/core';
import { Book, Basket, User } from '@models';
@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss'],
})
export class BookComponent implements OnInit {
  constructor() {}
  @Input() book;
  isAdminUser: boolean = false;
  ngOnInit() {
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) as User | null;
    console.log(currentUser);
    this.isAdminUser = currentUser && currentUser.isAdmin ? true : false;
  }

  addToBasket() {
    const basket = JSON.parse(localStorage.getItem('basket')) as Basket | null;
    const currentUser = JSON.parse(localStorage.getItem('currentUser')) as User | null;
    if (basket) {
      basket.books.push(this.book);
      if (currentUser) {
        basket.user = currentUser;
      }
      localStorage.setItem('basket', JSON.stringify(basket));
    } else {
      const nb: Basket = { books: [], checkedOut: [], user: null };
      nb.books.push(this.book);
      if (currentUser) {
        nb.user = currentUser;
      }
      localStorage.setItem('basket', JSON.stringify(nb));
    }
  }
}
