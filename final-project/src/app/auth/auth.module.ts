import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { CustomMaterialModule } from '../custom-material.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LogoutComponent } from './logout/logout.component';
import { TextMaskModule } from 'angular2-text-mask';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './auth/auth.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from '../shared/services/api.service';

@NgModule({
  declarations: [LoginComponent, RegisterComponent, LogoutComponent, AuthComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    AuthRoutingModule,
    CustomMaterialModule,
    TextMaskModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [ApiService],
})
export class AuthModule {}
