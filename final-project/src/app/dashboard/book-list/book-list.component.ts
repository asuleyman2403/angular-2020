import { Component, OnInit } from '@angular/core';
import { DataService } from '../../shared/services/data.service';
import { Book } from '@models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {

  constructor(private dataService: DataService, private router: Router) { }
  books: Book[] = [];
  ngOnInit() {
    this.books = this.dataService.allBooks;
  }

  removeBook(book: Book) {
    this.books = this.books.filter(item => item.id !== book.id);
    this.dataService.allBooks = this.books;
    this.dataService.filteredBookSource.next(this.dataService.allBooks);
    localStorage.setItem('books', JSON.stringify(this.dataService.allBooks));
  }

  editBook(book: Book) {
    this.router.navigate([`/dashboard/books/${book.id}`]);
  }
}
