import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) {}
  genres: string[] = [];
  form: FormGroup;

  ngOnInit() {
    const genres = JSON.parse(localStorage.getItem('genres'));
    if (genres) {
      this.genres = genres;
    }
    this.form = this.formBuilder.group({
      genre: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const { genre } = this.form.value;

    this.genres.push(genre);
    this.genres = Array.from(new Set(this.genres));
    localStorage.setItem('genres', JSON.stringify(this.genres));
  }

  remove(genre: string) {
    this.genres = this.genres.filter(item => item !== genre);
    localStorage.setItem('genres', JSON.stringify(this.genres));
  }
}
