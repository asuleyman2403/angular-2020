import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { BooksComponent } from './books/books.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { BookUpdateComponent } from './book-update/book-update.component';
import { AuthorsComponent } from './authors/authors.component';
import { GenresComponent } from './genres/genres.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'books',
        component: BooksComponent,
        children: [
          {
            path: '',
            component: BookListComponent,
          },
          {
            path: 'create',
            component: BookCreateComponent,
          },
          {
            path: ':id',
            component: BookUpdateComponent,
          },
        ],
      },
      {
        path: 'authors',
        component: AuthorsComponent,
      },
      {
        path: 'genres',
        component: GenresComponent,
      },
      {
        path: '**',
        redirectTo: 'books',
        pathMatch: 'full'
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class DashboardRoutingModule {}
