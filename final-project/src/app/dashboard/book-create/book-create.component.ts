import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Author, Book, Currency } from '@models';
import { v4 as uuv4 } from 'uuid';
import { DataService } from '../../shared/services/data.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.scss'],
})
export class BookCreateComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private dataService: DataService, private router: Router) {}
  authors: Author[] = [];
  genres: string[] = [];
  form: FormGroup;
  currencies: string[] = [Currency.KZT, Currency.RUB, Currency.USD];
  ngOnInit() {
    const authors = JSON.parse(localStorage.getItem('authors')) as Author[] | null;
    const genres = JSON.parse(localStorage.getItem('genres'));
    if (authors) {
      this.authors = authors;
    }
    if (genres) {
      this.genres = genres;
    }
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      imageUrl: ['', Validators.required],
      releaseDate: ['', Validators.required],
      cost: [0, Validators.required],
      currency: ['', Validators.required],
      author: ['', Validators.required],
      genres: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const { title, imageUrl, releaseDate, cost, currency, author, genres } = this.form.value;
    const nb: Book = {
      id: uuv4(),
      title,
      imageUrl,
      releaseDate,
      price: { cost, currency },
      author,
      genres,
    };
    this.dataService.allBooks.push(nb);
    this.dataService.filteredBookSource.next(this.dataService.allBooks);
    localStorage.setItem('books', JSON.stringify(this.dataService.allBooks));
    this.router.navigate(['dashboard/books']);
  }
}
