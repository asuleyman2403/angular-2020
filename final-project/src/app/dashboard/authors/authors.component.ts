import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Author } from '@models';
import { v4 as uuv4 } from 'uuid';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss'],
})
export class AuthorsComponent implements OnInit {
  constructor(private formBuilder: FormBuilder) {}
  authors: Author[] = [];
  form: FormGroup;

  ngOnInit() {
    const authors = JSON.parse(localStorage.getItem('authors')) as Author[] | null;
    if (authors) {
      this.authors = authors;
    }
    this.form = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    const { name, surname } = this.form.value;

    this.authors.push({
      id: uuv4(),
      name,
      surname,
    });
    localStorage.setItem('authors', JSON.stringify(this.authors));
  }

  remove(author: Author) {
    this.authors = this.authors.filter((item) => item.id !== author.id);
    localStorage.setItem('authors', JSON.stringify(this.authors));
  }
}
