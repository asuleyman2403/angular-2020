import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  constructor(private router: Router, private location: Location) {}
  links = [
    {
      title: 'Books',
      link: 'dashboard/books',
    },
    {
      title: 'Authors',
      link: 'dashboard/authors',
    },
    {
      title: 'Genres',
      link: 'dashboard/genres',
    },
  ];
  activeLink = this.links[0];
  ngOnInit() {
    const path = this.location.path();
    this.links.forEach((link) => {
      if (path === `/${link.link}`) {
        this.activeLink = link;
      }
    });
    this.router.events.subscribe(() => {
      const path = this.location.path();
      this.links.forEach((link) => {
        if (path === `/${link.link}`) {
          this.activeLink = link;
        }
      });
    });
  }

  onClick(link) {
    console.log(link);
    this.router.navigate([link]);
  }
}
