import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Author, Currency } from '@models';
import { DataService } from '../../shared/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.scss'],
})
export class BookUpdateComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}
  authors: Author[] = [];
  genres: string[] = [];
  form: FormGroup;
  currencies: string[] = [Currency.KZT, Currency.RUB, Currency.USD];
  id = '';
  ngOnInit() {
    const authors = JSON.parse(localStorage.getItem('authors')) as Author[] | null;
    const genres = JSON.parse(localStorage.getItem('genres'));
    if (authors) {
      this.authors = authors;
    }
    if (genres) {
      this.genres = genres;
    }
    this.form = this.formBuilder.group({
      title: ['', Validators.required],
      imageUrl: ['', Validators.required],
      releaseDate: ['', Validators.required],
      cost: [0, Validators.required],
      currency: ['', Validators.required],
      author: [{}, Validators.required],
      genres: ['', Validators.required],
    });
    this.route.params.subscribe((params) => {
      const id = params.id ? params.id : null;
      if (id) {
        const book = this.dataService.allBooks.find((item) => item.id === id);
        if (book) {
          this.id = id;
          const { title, imageUrl, releaseDate, price, author, genres } = book;
          this.form.patchValue({
            title,
            imageUrl,
            releaseDate,
            cost: price.cost,
            currency: price.currency,
            author,
            genres,
          });
          this.form.get('author').setValue(author);
          this.form.updateValueAndValidity();
        } else {
          this.router.navigate(['/dashboard/books']);
        }
      } else {
        this.router.navigate(['/dashboard/books']);
      }
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      return;
    }

    console.log(this.form.value);
    const { title, imageUrl, releaseDate, cost, currency, author, genres } = this.form.value;
    this.dataService.allBooks = this.dataService.allBooks.map((item) => {
      if (item.id === this.id) {
        return { id: item.id, title, imageUrl, releaseDate, price: { cost, currency }, author, genres };
      } else {
        return item;
      }
    });
    this.dataService.filteredBookSource.next(this.dataService.allBooks);
    localStorage.setItem('books', JSON.stringify(this.dataService.allBooks));
    this.router.navigate(['dashboard/books']);
  }
}
