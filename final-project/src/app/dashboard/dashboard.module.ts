import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { BooksComponent } from './books/books.component';
import { BookCreateComponent } from './book-create/book-create.component';
import { BookUpdateComponent } from './book-update/book-update.component';
import { BookListComponent } from './book-list/book-list.component';
import { AuthorsComponent } from './authors/authors.component';
import { GenresComponent } from './genres/genres.component';
import { CustomMaterialModule } from '../custom-material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DashboardRoutingModule } from './dashboard-routing.module';

@NgModule({
  declarations: [
    MainComponent,
    BooksComponent,
    BookCreateComponent,
    BookUpdateComponent,
    BookListComponent,
    AuthorsComponent,
    GenresComponent,
  ],
  imports: [CommonModule, DashboardRoutingModule, ReactiveFormsModule, CustomMaterialModule],
})
export class DashboardModule {}
