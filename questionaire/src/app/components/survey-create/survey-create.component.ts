import { Component, OnInit } from "@angular/core";
import { FormGroup, FormArray, FormBuilder, Validators } from "@angular/forms";
import { Question, Survey } from "../../types/survey";
@Component({
  selector: "app-survey-create",
  templateUrl: "./survey-create.component.html",
  styleUrls: ["./survey-create.component.scss"],
})
export class SurveyCreateComponent implements OnInit {
  form: FormGroup;
  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.form = this.formBuilder.group({
      amountOfQuestions: [0, [Validators.required, Validators.min(0)]],
      questions: this.formBuilder.array([]),
    });
    const amountOfQuestionsController = this.form.get("amountOfQuestions");
    amountOfQuestionsController.valueChanges.subscribe((data) => {
      if (amountOfQuestionsController.valid) {
        this.handleAmountChanges(data);
      }
    });
    const form = JSON.parse(localStorage.getItem("surveyForm"));
    if (form) {
      this.form.patchValue(form);
    }

    this.form.valueChanges.subscribe((data) => {
      localStorage.setItem("surveyForm", JSON.stringify(this.form.value));
    });
  }

  onSubmit(values) {
    const questions = values.questions.map((item) => {
      const variants = item.variants.map((variant) => variant.title);
      const answers = item.variants
        .filter((variant) => variant.isAnswer)
        .map((variant) => variant.title);
      return {
        title: item.title,
        time: item.time,
        variants,
        answers,
      };
    });
    const survey = {
      amountOfQuestions: values.amountOfQuestions as number,
      questions,
    };
    localStorage.setItem("survey", JSON.stringify(survey));
  }

  get questions() {
    return this.form.get("questions") as FormArray;
  }

  handleAmountChanges(data: any) {
    if (this.questions.length < data) {
      const diff = data - this.questions.length;
      for (let i = 0; i < diff; i++) {
        this.addQuestionController();
      }
    } else {
      const diff = this.questions.length - data;
      for (let i = 0; i < diff; i++) {
        this.removeQuestionController();
      }
    }
  }

  addQuestionController() {
    const questionGroup = this.formBuilder.group({
      title: ["", [Validators.required]],
      time: [0, [Validators.required, Validators.min(5)]],
      variants: this.formBuilder.array([]),
    });
    const variants = questionGroup.get("variants") as FormArray;
    for (let i = 0; i < 4; i++) {
      const variantGroup = this.formBuilder.group({
        title: ["", Validators.required],
        isAnswer: [false],
      });
      variants.push(variantGroup);
    }
    this.questions.push(questionGroup);
  }

  removeQuestionController() {
    this.questions.removeAt(this.questions.length - 1);
  }

  deleteExactQuestionController(index: number) {
    this.questions.removeAt(index);
    this.form.get("amountOfQuestions").setValue(this.questions.length);
  }

  addVariantToQuestion(index: number) {
    const variantGroup = this.formBuilder.group({
      title: ["", Validators.required],
      isAnswer: [false],
    });
    const variants = this.questions.controls[index].get(
      "variants"
    ) as FormArray;
    variants.push(variantGroup);
  }

  deleteExactVariantOfQuestion(qIndex: number, varIndex: number) {
    const variants = this.questions.controls[qIndex].get(
      "variants"
    ) as FormArray;
    if (variants.length > 2) {
      variants.removeAt(varIndex);
    }
  }
}
