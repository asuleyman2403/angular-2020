import { Component, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, FormArray, FormControl } from "@angular/forms";

@Component({
  selector: "app-survey-fill",
  templateUrl: "./survey-fill.component.html",
  styleUrls: ["./survey-fill.component.scss"],
})
export class SurveyFillComponent implements OnInit, OnDestroy {
  form: FormGroup;
  survey: any;
  overallSurveyTime = 0;
  interval: any;
  timeString = "00 : 00";
  textRed = false;
  constructor(private formBuilder: FormBuilder, private router: Router) {}
  ngOnInit() {
    const survey = JSON.parse(localStorage.getItem("survey"));
    if (!survey) {
      this.router.navigate(["/create"]);
    }
    this.survey = survey;
    this.overallSurveyTime = this.survey.questions.reduce(
      (acc, item) => acc + item.time,
      0
    );
    const time = localStorage.getItem("time");
    if (time && !Number.isNaN(Number(time))) {
      this.overallSurveyTime = Number(time) > 0 ? Number(time) : 0;
    }
    this.interval = setInterval(() => {
      const seconds = this.overallSurveyTime % 60;
      const mins = (this.overallSurveyTime - seconds) / 60;
      this.timeString = `${
        mins > 9 ? mins : `0${mins} : ${seconds > 9 ? seconds : `0${seconds}`}`
      }`;
      if (this.overallSurveyTime === 0) {
        this.onSubmit(this.form.value);
        clearInterval(this.interval);
      }
      if (this.overallSurveyTime < 60) {
        this.textRed = true;
      }
      this.overallSurveyTime--;
    }, 1000);

    this.form = this.formBuilder.group({
      questions: this.formBuilder.array([]),
    });
    this.loadQuestions();
    const form = JSON.parse(localStorage.getItem("answerForm"));
    if (form) {
      this.form.patchValue(form);
    }

    this.form.valueChanges.subscribe(() => {
      localStorage.setItem("answerForm", JSON.stringify(this.form.value));
    });
  }

  get questions() {
    return this.form.get("questions") as FormArray;
  }

  loadQuestions() {
    this.survey.questions.forEach((item) => {
      if (item.answers.length > 1) {
        const { variants } = item;
        const varOptions = variants.reduce((acc, variant) => {
          return { ...acc, [variant]: false };
        }, {});
        const questionGroup = this.formBuilder.group(varOptions);
        this.questions.push(questionGroup);
      } else {
        const questionController = new FormControl("");
        this.questions.push(questionController);
      }
    });
  }

  onSubmit(values) {
    let score = 0;
    const maxScore = values.questions.length;
    const scoreForEach = [];
    values.questions.forEach((item, index) => {
      let sc = 0;
      if (typeof item === "string" && item.length) {
        sc += this.survey.questions[index].answers.includes(item) ? 1 : 0;
      } else {
        const answers = Object.keys(item).filter(
          (key) =>
            item[key] && this.survey.questions[index].answers.includes(key)
        );
        sc += answers.length / this.survey.questions[index].answers.length;
      }
      scoreForEach.push({
        title: this.survey.questions[index].title,
        score: sc,
      });
      score += sc;
    });
    const result = {
      overall: Math.round((score / maxScore) * 100),
      scoreForEach,
    };
    localStorage.setItem("result", JSON.stringify(result));
    this.router.navigate(["/results"]);
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
    localStorage.setItem("time", JSON.stringify(this.overallSurveyTime));
  }
}
