import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
@Component({
  selector: "app-survey-results",
  templateUrl: "./survey-results.component.html",
  styleUrls: ["./survey-results.component.scss"],
})
export class SurveyResultsComponent implements OnInit {
  tableData = [];
  overall = 0;
  displayedColumns: string[] = ["title", "score"];
  constructor(private router: Router) {}

  ngOnInit() {
    const result = JSON.parse(localStorage.getItem("result"));
    if (!result) {
      this.router.navigate(["/fills"]);
    }
    this.overall = result.overall;
    this.tableData = result.scoreForEach;
  }
}
