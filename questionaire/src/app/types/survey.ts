export interface Question {
    title: string;
    variants: string[];
    answers: string[];
    timeInSeconds: number;
}

export interface Survey {
    amountOfQuestionss: number;
    questions: Question;
}
