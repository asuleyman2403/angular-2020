import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SurveyCreateComponent } from "./components/survey-create/survey-create.component";
import { SurveyFillComponent } from "./components/survey-fill/survey-fill.component";
import { SurveyResultsComponent } from "./components/survey-results/survey-results.component";

const routes: Routes = [
  {
    path: "create",
    component: SurveyCreateComponent,
  },
  {
    path: "fill",
    component: SurveyFillComponent,
  },
  {
    path: "results",
    component: SurveyResultsComponent,
  },
  {
    path: "",
    redirectTo: "create",
    pathMatch: "full",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
