import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  title = "questionaire";
  links = ["create", "fill", "results"];
  activeLink = "create";
  constructor(private router: Router, private location: Location) {}

  ngOnInit() {
    this.router.events.subscribe(() => {
      const path = this.location.path();
      if (path === "/create") {
        this.activeLink = "create";
      }
      if (path === "/fill") {
        this.activeLink = "fill";
      }
      if (path === "/results") {
        this.activeLink = "results";
      }
    });
  }
}
