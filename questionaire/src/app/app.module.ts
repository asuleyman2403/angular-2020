import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './custom-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SurveyCreateComponent } from './components/survey-create/survey-create.component';
import { SurveyFillComponent } from './components/survey-fill/survey-fill.component';
import { SurveyResultsComponent } from './components/survey-results/survey-results.component';

@NgModule({
  declarations: [
    AppComponent,
    SurveyCreateComponent,
    SurveyFillComponent,
    SurveyResultsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
